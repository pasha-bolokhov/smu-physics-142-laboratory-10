%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                  H E A D E R                                 %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{header}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                D O C U M E N T                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\def\psep{4.0cm}
\def\qsep{3.0cm}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                                                              %
%                                   T I T L E                                  %
%                                                                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{center}
{  \huge \bf \color{red} Laboratory X  }\\[2mm]
{  \Large \textbf{\textit{\color{red} Oscilloscopes}}  }\\[2mm]
{  \large \it \color{azure(colorwheel)} Introductory Physics II --- Physics 142/172L  }
\end{center}
\hrule
\bigskip
\begin{tabularx}{\textwidth}{lXr}
%
	&
	{\bf Name:}
	&
	{\bf Laboratory Section:}
	\hspace{1.0cm}
	\\[2mm]
%
	&
	{\bf Partners:}\hspace{5.0cm}
\end{tabularx}
\medskip


%\def\SHOWSOLUTIONS{1}
%\newcommand\solution[1]{{\ifdefined\SHOWSOLUTIONS\color{richelectricblue}#1\fi}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                            I N T R O D U C T I O N                           %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Introduction}}

	In this laboratory we will work with the oscilloscope --- a very useful tool
	for visualizing the workings of various electronic circuits.
	In class we will familiarize ourselves with the controls and settings of an oscilloscope.
	We will learn how to measure the voltage of DC and AC power sources,
	as well as observe the wave patterns of various signals

\bigskip\noindent
	In its most common usage, an oscilloscope provides an electronic ``graph''
	of the circuit operations with time on the horizontal axis, and voltage on the vertical axis.
	The scale of both axes can be adjusted as required for best display

\bigskip\noindent
	Oscilloscopes are like many scientific instruments.
	They are simple conceptually but complicated in their engineering

\bigskip\noindent
	The type of oscilloscope we will be using has a CRT display --- Cathode Ray Tube ---
	which is essentially the same as the picture tube in an older television or computer monitor

\begin{center}
\includegraphics[width=10cm]{hp-oscilloscope-crt.jpg}
\end{center}

\bigskip\noindent
	The tube is a large evacuated glass tube with electronics in the small end to form
	a beam of high speed electrons which travel forward and hit the inside surface of the large end (screen)
	which is coated with a phosphorescent material.
	This causes a bright spot where the beam dumps energy

\begin{center}
\includegraphics[width=14cm]{crt-diagram.png}
\end{center}

\bigskip\noindent
	Let us now explain more about how a CRT works.
	A large current is passed through a filament (causing it to glow --- to heat up).
	This gives the electrons in the filament enough kinetic energy for them to leave the filament
	(to boil off).
	The electrons are then attracted to a positively charged plate with a hole in it.
	The electrons pass through the hole to form the beam.
	The beam is given a timed horizontal deflection which causes it to sweep across the screen
	of the oscilloscope at a measured rate

\bigskip\noindent
	Finally, an input is fed to vertical deflection plates to display a graphic representation
	of the vertical input versus tie on the horizontal.
	The horizontal deflection (sweep rate) can be adjusted slow enough that you can watch the beam move
	across the screen,
	or fast enough to display a rapidly changing input.
	Additional information on this topic may be found in your textbook and on the web


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                               P R O C E D U R E                              %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Procedure}}

\renewcommand{\thesubsection}{\arabic{subsection}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     M E A S U R E   D C   V O L T A G E S                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Measure DC voltages}}
	Use the oscilloscope to measure DC voltages
	and compare them to the multi--meter readings

\begin{itemize}
\item
	For the first measurement, adjust the DC power source to \cc{ \SI{5}\volt }

\item[$\cdot$]
	Ensure that the calibration knob \emph{VAR} is fully \emph{CW}

\item[$\cdot$]
	Set the coupling switch of the oscilloscope to \emph{GND}.
	The coupling switch has positions for \emph{AC}, \emph{DC} and \emph{GND},
	and is just to the left of the \emph{V/DIV} knob

\item[$\cdot$]
	Adjust the vertical display so the horizontal line is on one of the divisions

\item[$\cdot$]
	Switch the coupling to \emph{DC}.
	Connect the oscilloscope and adjust the \emph{V/DIV} to read the voltage
	(vertical position shift).

\item[\color{alizarin}\ding{234}]
	Switch the coupling to \emph{AC}.
	What happens?
\end{itemize}

\noindent
	Set the power source to several other voltages and practice measuring voltages
	with the oscilloscope.
	Check your measurements with the multi--meter.
\begin{itemize}
\item[\color{alizarin}\ding{234}]
	How close are your oscilloscope measurements to what the multi--meter reads?
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                     M E A S U R E   A C   V O L T A G E S                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Measure AC voltages}}

\begin{itemize}
\item[$\cdot$]
	Use the multi--meter to measure the \emph{12VAC} output from the power source.
	Record the voltage

\item[$\cdot$]
	Connect the oscilloscope to the \emph{12VAC} output.
	Adjust your horizontal and vertical scale as needed to view the sine wave.
	You may want to use an \emph{X10} probe to display the sine wave.
	\emph{Alternatively}: you could set the coupling to \emph{GND},
	adjust the trace to place it at the lowest line,
	then switch the coupling to \emph{AC} and measure the positive peak voltage;
	switch the coupling back to \emph{GND} and move the trace to the top
	to measure the negative peak voltage in a similar fashion

\item[\color{alizarin}\ding{234}]
	How does the wave differ from your expectations?
	Theoretically, there is a \cc{ 1/\sqrt{2} } ratio of the Root Mean Square voltage
	to peak voltage for a sine wave.
	Does this match the ratio you saw?
	If not, why?
\end{itemize}


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                      O B S E R V E   W A V E   F O R M S                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Observe the wave forms of a signal generator}}

\begin{itemize}
\item[$\cdot$]
	Connect the signal generator to the oscilloscope
	
\item[$\cdot$]
	Adjust the controls for a clear display of the signal generator output

\item[$\cdot$]
	Change the signal generator to the different waveform output options.
	Does the display match the picture on the signal generator?

\item[$\cdot$]
	Switch the signal generator to a sine wave pattern

\item[\color{alizarin}\ding{234}]
	Measure the period (time) of the sine wave using the scale
	on the screen.
	Make sure the oscilloscope is calibrated
	(\emph{Sweep VAR} knob fully \emph{CW})

\item[\color{alizarin}\ding{234}]
	Using the relationship between time (period) and frequency,
	calculate the frequency of the sine wave you are displaying

\item[\color{alizarin}\ding{234}]
	Check this against the multi--meter's reading of the frequency
\end{itemize}

\noindent
	Do this for several representative frequencies over the range of the
	signal generator


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        O B S E R V E   W A V E   F O R M S   O F   R E C T I F I E R S       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Observe wave forms of rectifier circuits}}
	``Rectifying'' AC voltage is the first step in converting it to DC,
	which is what virtually all electronic devices require for operation.
	In electronic circuits, ``rectifying'' eliminates the negative (or positive)
	half cycle of AC voltage, resulting in ``pulsating DC''.
	There are two types of rectifier circuits set up on the circuit boards.
\begin{itemize}
\item[$\cdot$]
	Connect the \emph{12VAC} output of your power source to the input of the
	circuit boards (one circuit at a time)

\item[$\cdot$]
	\emph{Use \emph{DC} coupling on your oscilloscope input}

\item[$\cdot$]
	Set your oscilloscope to display the sine wave at the output of the power source
	(input of the circuit board)

\item[$\cdot$]
	Without changing your oscilloscope settings, move the oscilloscope leads
	to connect the oscilloscope to the \emph{output} of the circuit

\item[\color{alizarin}\ding{234}]
	How does the wave pattern differ from the AC input?

\item[\color{alizarin}\ding{234}]
	How do the output wave forms differ between the two rectifier
	circuits?
	(it is important to note the time and voltage scale settings
	and the corresponding measurements to accurately note the difference)

\item[\color{alizarin}\ding{234}]
	Sketch the input and output wave forms to scale
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              O B S E R V E   L I S S A J O U S   P A T T E R N S             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{\textit{Observe Lissajous patterns}}

	Lissajous patterns have a variety of uses in electronics and science fiction movies.
	This requires a different set up of the oscilloscope from what we have been using up until now.
	Instead of a time--calibrated horizontal display,
	and a voltage--calibrated vertical display,
	both the horizontal and vertical sweeps are driven by the outputs of signal generators

\begin{itemize}
\item[$\cdot$]
	Select the \emph{X--Y} option near the upper right of your oscilloscope panel
	and on the \emph{vertical mode} switch between the two \emph{V/DIV} knobs

\item[$\cdot$]
	Connect one signal generator to \emph{Channel 1} and another to \emph{Channel 2} of your oscilloscope

\item[$\cdot$]
	Adjust the signal generators to produce various (stable)
	patterns of loops on your oscilloscope

\item[\color{alizarin}\ding{234}]
	Draw the patterns you obtain

\item[\color{alizarin}\ding{234}]
	What is the relationship (ratio) between the frequencies of your horizontal and vertical inputs
	for each pattern you create?
	Reduce ratios to lowest common denominators, single digits, rounding as needed ---
	\cc{ 1:3 }, \cc{ 4:1 } \emph{etc}
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                E X A M P L E S                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection*{\textit{Examples}}

	You can refer to the beautiful sample Lissajous figures below for your analysis.
	You still need to identify the ratios of the frequencies on the pictures you find
	by following the line on the figure

\bigskip
\def\lisssamples{1000}
\begin{center}
\begin{tabular}{c @{\hskip 2.0cm} c @{\hskip 2.0cm} c}
%
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(1 * \t r)}, {2 * cos(1 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(1 * \t r)}, {2 * sin(2 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(1 * \t r)}, {2 * cos(3 * \t r)});
\end{tikzpicture}
	\\[2mm]
%
	1:1
	&
	1:2
	&
	1:3
	\\[8mm]
%
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4, id = one-one] ({2 * sin(1 * \t r)}, {2 * sin(4 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(1 * \t r)}, {2 * cos(5 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(2 * \t r)}, {2 * sin(3 * \t r)});
\end{tikzpicture}
	\\[2mm]
%
	1:4
	&
	1:5
	&
	2:3
\end{tabular}
\end{center}
\begin{center}
\begin{tabular}{c @{\hskip 2.0cm} c @{\hskip 2.0cm} c}
	\\[8mm]
%
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4, id = one-one] ({2 * sin(2 * \t r)}, {2 * sin(5 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(3 * \t r)}, {2 * sin(4 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(3 * \t r)}, {2 * cos(5 * \t r)});
\end{tikzpicture}
	\\[2mm]
%
	2:5
	&
	3:4
	&
	3:5
	\\[8mm]
%
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4, id = one-one] ({2 * sin(3 * \t r)}, {2 * cos(7 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(4 * \t r)}, {2 * sin(5 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(4 * \t r)}, {2 * sin(7 * \t r)});
\end{tikzpicture}
	\\[2mm]
%
	3:7
	&
	4:5
	&
	4:7
	\\[8mm]
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4, id = one-one] ({2 * sin(5 * \t r)}, {2 * sin(6 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(5 * \t r)}, {2 * cos(7 * \t r)});
\end{tikzpicture}
&
\begin{tikzpicture}
	\draw [draw = coquelicot, thick, variable = \t]
		plot [samples = \lisssamples, domain = -4:4] ({2 * sin(5 * \t r)}, {2 * sin(8 * \t r)});
\end{tikzpicture}
	\\[2mm]
%
	5:6
	&
	5:7
	&
	5:8
\end{tabular}
\end{center}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                                A N A L Y S I S                               %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Analysis}}

	Discuss the results of your work with an oscilloscope.
	Be sure to address the questions raised in each task mark with an ~{\color{alizarin}\ding{234}}~ symbol


\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                              %
%                   L A B O R A T O R Y   E V A L U A T I O N                  %
%                                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\textit{Laboratory evaluation}}

	Please provide feedback on the following areas, comparing this laboratory to your previous labs.
	Please assign each of the listed categories a value in 1 ~--~ 5, with 5 being the best, 1 the worst.

\bigskip
\begin{itemize}
\item
	how much fun you had completing this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well the lab preparation period explained this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the amount of work required compared to the time allotted?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	your understanding of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	the difficulty of this laboratory?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5

\item
	how well this laboratory tied in with the lecture?
	\hfill 1\qquad 2\qquad 3\qquad 4\qquad 5
\end{itemize}

\bigskip\noindent
	Comments supporting or elaborating on your assessment can also be very helpful in improving the future laboratories


\end{document}
